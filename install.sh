INSTALL_DIR=/opt/mqtt_commander
CONFIG_DIR=/etc/mqtt_commander
mkdir $INSTALL_DIR
cp mqtt_commander.py $INSTALL_DIR
cp mqtt_commander.service /etc/systemd/system

mkdir $CONFIG_DIR
cp -R etc/mqtt_commander/* $CONFIG_DIR/
systemctl daemon-reload
