from asyncio.windows_events import NULL
import getopt
import configparser
import sys
import os
import socket

broker = ""
port = -1 
root = ""
ca_file = ""
cert_file = ""
key_file = ""
use_tls = True
commands = NULL
status_points = NULL


def checkRequiredFile(filename):
    if os.path.isfile(filename) == False:
        print("[ERROR] File not found: " + filename)
        sys.exit(-1)


def ConfigSectionMap(Config, section):
    dict1 = {}
    options = Config.options(section)
    for option in options:
        try:
            dict1[option] = Config.get(section, option)
            if dict1[option] == -1:
                print("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None
    return dict1

def Init():
    Config = configparser.ConfigParser()
    
    argumentList = sys.argv[1:]
    configPath = "/etc/mqtt_commander/mqtt_commander.conf"
    #configPath = ".\etc\mqtt_commander\mqtt_commander.conf"
    # Options
    options = "hc:"
    long_options = ["help", "config"]

    try:
        # Parsing argument
        arguments, values = getopt.getopt(argumentList, options, long_options)
        
        # checking each argument
        for currentArgument, currentValue in arguments:
    
            if currentArgument in ("-h", "--help"):
                print ("usage: python mqtt_commander.py [-c config_filename]")
                
            elif currentArgument in ("-c"):
                configPath = currentValue
                
    except getopt.error as err:
        # output error, and return with an error code
        print ("[ERROR]" + str(err))

    print("[INFO] Using configuration file: " + configPath )



    checkRequiredFile(configPath)

    Config.read(configPath)

    Config.sections()

    
    global broker
    global port 
    global root
    global ca_file
    global cert_file
    global key_file
    global use_tls
    global commands
    global status_points

    broker = ConfigSectionMap(Config, "broker")['host']
    port = int(ConfigSectionMap(Config, "broker")['port'])

    print("[INFO] Broker: " + broker + ":" + str(port))

    root = ConfigSectionMap(Config, "topics")['root'] + '/' + socket.gethostname() + "/"

    ca_file = ConfigSectionMap(Config,"broker")['ca']
    cert_file = ConfigSectionMap(Config,"broker")['cert']
    key_file = ConfigSectionMap(Config,"broker")['key']
    use_tls = ConfigSectionMap(Config,"broker")['tls'].lower() == "true"


    commands = ConfigSectionMap(Config,"commands")
    status_points = ConfigSectionMap(Config,"status")

    if use_tls:
        checkRequiredFile(ca_file)
        checkRequiredFile(cert_file)
        checkRequiredFile(key_file)