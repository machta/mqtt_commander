from asyncio.windows_events import NULL
import sys


import time
import ssl
from os.path import exists
import os
import atexit
import signal

import depcheck
import configuration

depcheck.checkDependencies()
configuration.Init()


connected = False

from paho.mqtt import client as mqtt_client

client = mqtt_client.Client()

def status_update(client):
    for point in configuration.status_points.keys():
        stream = os.popen(configuration.status_points[point])
        output = stream.read()
        client.publish(configuration.root + "status/" + point, output, qos=0,retain=True)

def on_message(client, userdata, msg):
    payload = msg.payload.decode()
    request = msg.topic.replace(configuration.root, "")
    print("[INFO] Received request: " + request + ": " + payload)

    if request == "command":
        if payload in configuration.commands: 
            print("[INFO] Result: OK " + payload)
            stream = os.popen(configuration.commands[payload])
            output = stream.read()
            client.publish(configuration.root + "result", output,qos=0,retain=True)
            status_update(client)
        else:
            client.publish(configuration.root + "result", "[ERROR] Wrong command",qos=0,retain=True)
            print("[ERROR] Wrong command")


            
client.on_message = on_message

def on_connect(client, userdata, flags, rc):
    global connected  # Use global variable
    if rc == 0:

        print("[INFO] Connected to broker")
        connected = True  # Signal connection
        client.subscribe(configuration.root + "command")
        print("[INFO] Subscribed to: " + configuration.root)
    else:
        print("[INFO] Error, connection failed")


def on_publish(client, userdata, result):
    print("[INFO] Published: " + userdata)

def connect(mqtt_client, broker_endpoint, port, use_tls, ca_file, cert_file, key_file):
    global connected

    if not mqtt_client.is_connected():
        #mqtt_client.username_pw_set(mqtt_username, password=mqtt_password)
        mqtt_client.on_connect = on_connect
        #mqtt_client.on_publish = on_publish
        if use_tls:
            mqtt_client.tls_set(ca_certs=ca_file, certfile=cert_file,
                            keyfile=key_file, cert_reqs=ssl.CERT_REQUIRED,
                            tls_version=ssl.PROTOCOL_TLSv1_2)
            mqtt_client.tls_insecure_set(False)
        mqtt_client.connect(broker_endpoint, port=port)
        mqtt_client.loop_start()

        attempts = 0
        while not connected and attempts < 5:  # Wait for connection
            print("[INFO] Attempting to connect...")
            time.sleep(1)
            attempts += 1

    if not connected:
        print("[ERROR] Could not connect to broker")
        return False

    return True


def handle_close(client):
    client.publish(configuration.root + "status", "offline", qos=0,retain=True)
    time.sleep(1)

def sigterm_handler(_signo, _stack_frame):
    client.publish(configuration.root + "status", "offline", qos=0,retain=True)
    time.sleep(1)
    sys.exit(0)

connect(client, configuration.broker, configuration.port, configuration.use_tls, configuration.ca_file, configuration.cert_file, configuration.key_file)


client.publish(configuration.root + "status", "online", qos=0,retain=True)

atexit.register(handle_close, client)
signal.signal(signal.SIGTERM, sigterm_handler)
#signal.signal(signal.SIGKILL, sigterm_handler)

while True:
    status_update(client)
    time.sleep(60)

